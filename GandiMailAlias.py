#! /usr/bin/env python
# -*- coding: utf-8 -*-

try:
	from xmlrpc.client import ServerProxy as _ServerProxy
except ImportError:
	from xmlrpclib import ServerProxy as _ServerProxy

__server = None

def server(endpoint):
	global __server
	__server = _ServerProxy(endpoint)
	if args.debug:
		print('endpoint: ' + endpoint + ' : ' + str(__server))

def __get_aliases(apikey, domain, login):
	global __server
	return __server.domain.mailbox.info(apikey, domain, login)['aliases']

def list_account(apikey, domain, login, sort=False):
	print(login + '@' + domain + ':')
	aliases = __get_aliases(apikey, domain, login)
	if sort:
		aliases.sort()
	if len(aliases) > 0:
		for alias in aliases:
			print("\t" + alias)
	else:
		print("\t(No alias)")
	print('')

def add_alias(apikey, domain, login, alias, sort=False):
	global __server
	aliases = __get_aliases(apikey, domain, login)
	if not alias in aliases:
		print('Adding ' + alias + ' in ' + login + '@' + domain)
		aliases.append(alias)
		__server.domain.mailbox.alias.set(apikey, domain, login, aliases)
	else:
		print(alias + ' exists in ' + login + '@' + domain)
	print('')
	list_account(apikey, domain, login, sort)

def remove_alias(apikey, domain, login, alias, sort=False):
	global __server
	aliases = __get_aliases(apikey, domain, login)
	if alias in aliases:
		print('Removing ' + alias + ' in ' + login + '@' + domain)
		aliases.remove(alias)
		__server.domain.mailbox.alias.set(apikey, domain, login, aliases)
	else:
		print(alias + ' does not exists in ' + login + '@' + domain)
	print('')
	list_account(apikey, domain, login, sort)


# Execute as a command
if __name__ == '__main__':
	# Some import required in this case
	import os
	import argparse
	import json

	parser = argparse.ArgumentParser()
	# Optional arguments
	parser.add_argument("-f", "--file", dest="rcfile", default=os.getenv("HOME")+"/.gandimailalias.rc", help="Setting file")
	parser.add_argument("-d", "--debug", dest="debug", action="store_true", help="Show debug messages")
	parser.add_argument("-t", "--test", dest="test", action="store_true", help="Use the test API endpoint")
	parser.add_argument("-s", "--sort", dest="sort", action="store_true", help="Sort alias alphabeticaly")
	# List command
	subparsers = parser.add_subparsers(dest="action", title="Available actions")
	parser_list = subparsers.add_parser("list", help="List aliases")
	parser_list.add_argument("account", nargs="?", help="Limit output to account")
	# Add command
	parser_add = subparsers.add_parser("add", help="Add alias")
	parser_add.add_argument("account", help="Account where to add the alias")
	parser_add.add_argument("alias", help="Alias to be added")
	# Remove command
	parser_remove = subparsers.add_parser("remove", help="Remove alias")
	parser_remove.add_argument("account", help="Remove an alias from an account")
	parser_remove.add_argument("alias", help="Alias to be removed")
	# Delete command
	parser_delete = subparsers.add_parser("delete", help="Same as remove")
	parser_delete.add_argument("account", help="Remove an alias from account")
	parser_delete.add_argument("alias", help="Alias to be removed")
	# Parse arguments
	args = parser.parse_args()

	# Read a config file (JSON formated)
	settings_f = open(args.rcfile, 'r')
	settings = json.load(settings_f)
	settings_f.close()
	if args.debug:
		print('settings: ' + str(settings))

	# XML-RPC server
	if args.test:
		server("https://rpc.ote.gandi.net/xmlrpc/")
	else:
		server("https://rpc.gandi.net/xmlrpc/")

	# Get the apikey, domain and login for an account
	def __get_account(account):
		apikey = settings[account]
		login, domain = account.split('@')
		if args.debug:
			print('login: ' + login + '\tdomain: ' + domain + '\tapikey: ' + apikey)
		return apikey, login, domain

	if args.account:
		apikey, login, domain = __get_account(args.account)

	# Get the action
	if args.action == 'list':
		if args.account:
			list_account(apikey, domain, login, args.sort)
		else:
			for account in settings:
				apikey, login, domain = __get_account(account)
				list_account(apikey, domain, login, args.sort)
	elif args.action == 'add':
		add_alias(apikey, domain, login, args.alias, args.sort)
	elif args.action == 'remove' or args.action == 'delete':
		remove_alias(apikey, domain, login, args.alias, args.sort)
